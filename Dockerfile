FROM rust:alpine as build
RUN apk add build-base
WORKDIR /root
RUN mkdir -p graphql src templates
COPY Cargo.toml Cargo.lock ./
COPY graphql/ graphql/
COPY src/ src/
COPY templates/ templates/
RUN cargo install --path . --locked --target=x86_64-unknown-linux-musl; cargo clean

FROM alpine:3.19
COPY --from=build /usr/local/cargo/bin/trawler-stats /root
