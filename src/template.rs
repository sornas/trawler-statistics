use askama::Template;

#[derive(Template, Debug)]
#[template(path = "index.html")]
pub struct Index {
    pub title: String,
    pub mrs: Vec<IndexMr>,
    pub commits: Vec<IndexCommit>,
    pub generated: String,
}

#[derive(Debug)]
pub struct IndexMr {
    pub id: String,
    pub title: String,
    pub report: Report,
    pub trawler: String,
    pub trawler_date: String,
    pub error: String,
}

#[derive(Debug)]
pub struct IndexCommit {
    pub title: String,
    pub sha: String,
    pub report: Report,
    pub trawler: String,
    pub trawler_date: String,
    pub error: String,
}

#[derive(Debug, Clone)]
pub struct Report {
    pub build: String,
    pub test: String,
    pub pnr: String,
}

#[derive(Template, Clone)]
#[template(path = "details.html")]
pub struct Details {
    pub title: String,
    pub external_link: String,
    pub generated: String,
    pub cases: Vec<DetailsCase>,
}

#[derive(Clone)]
pub struct DetailsCase {
    pub name: String,
    pub id: String,
    pub steps: Vec<DetailsCaseStep>,
    pub steps_success: u32,
    pub steps_total: u32,
}

#[derive(Clone)]
pub struct DetailsCaseStep {
    pub name: String,
    pub success: bool,
    pub output: String,
}
