use askama::Template as _;
use futures::future::join_all;
use lazy_static::lazy_static;
use tracing::Level;
use tracing_subscriber::{filter, prelude::*};

use crate::template::{Index, IndexCommit, IndexMr};

lazy_static! {
    pub static ref GENERATED: String =
        chrono::offset::Utc::now().to_rfc3339_opts(chrono::SecondsFormat::Secs, true);
}

pub mod api;
pub mod template;

async fn index(mrs: Vec<IndexMr>, commits: Vec<IndexCommit>) {
    let template = Index {
        title: "Trawler statistics".to_string(),
        mrs,
        commits,
        generated: GENERATED.clone(),
    };
    let content = smol::unblock(move || template.render().unwrap()).await;
    smol::fs::write("site/index.html", content).await.unwrap();
}

fn main() {
    tracing_subscriber::registry()
        .with(tracing_forest::ForestLayer::default())
        .with(
            filter::Targets::new()
                .with_default(Level::WARN)
                .with_target("trawler_stats", Level::DEBUG),
        )
        .init();
    let job_token = std::env::var("JOB_TOKEN").ok();
    let job_token = job_token.as_deref();

    let _ = smol::block_on(async move {
        smol::fs::create_dir_all("site").await.unwrap();

        let ((mrs, mr_details), (commits, commit_details)) = futures::join!(
            api::OpenMrDownstreamPipelines::get(job_token.clone()),
            api::CommitDownstreamPipelines::get(job_token),
        );

        futures::join!(
            index(mrs, commits),
            join_all(mr_details.iter().chain(commit_details.iter()).map(
                |(link, details)| async move {
                    smol::fs::write(format!("site/{link}.html"), details.render().unwrap())
                        .await
                        .unwrap();
                }
            )),
        );
    });
}
