use std::collections::HashMap;

use graphql_client::{GraphQLQuery, Response};
use itertools::Itertools;
use serde::Deserialize;
use tap::Pipe;

use crate::template::{Details, DetailsCase, DetailsCaseStep, IndexCommit, IndexMr, Report};
use crate::GENERATED;

type Time = String;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/gitlab-schema-20240308.json",
    query_path = "graphql/gitlab-open-mr-downstream-pipelines.graphql",
    response_derives = "Debug"
)]
pub struct OpenMrDownstreamPipelines;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/gitlab-schema-20240308.json",
    query_path = "graphql/gitlab-commit-downstream-pipelines.graphql",
    response_derives = "Debug"
)]
pub struct CommitDownstreamPipelines;

impl OpenMrDownstreamPipelines {
    #[tracing::instrument(name = "OpenMrDownstreamPipelines::get", skip(job_token))]
    pub async fn get(job_token: Option<&str>) -> (Vec<IndexMr>, Vec<(String, Details)>) {
        let query = Self::build_query(open_mr_downstream_pipelines::Variables {});
        let job_token_ = job_token.map(|s| s.to_string());
        let resp: Response<open_mr_downstream_pipelines::ResponseData> = smol::unblock(move || {
            ureq::post("https://gitlab.com/api/graphql")
                .pipe(|p| {
                    if let Some(job_token) = job_token_ {
                        p.set("JOB_TOKEN", &job_token)
                    } else {
                        p
                    }
                })
                .send_json(query)
                .unwrap()
                .into_string()
                .unwrap()
                .pipe_ref(|p| serde_json::from_str(p))
                .unwrap()
        })
        .await;
        let mrs = resp
            .data
            .unwrap()
            .project
            .unwrap()
            .merge_requests
            .unwrap()
            .nodes
            .unwrap()
            .into_iter()
            .map(|mr| async {
                let mr = mr.expect("no mr");
                let downstream = mr
                    .pipelines
                    .expect("no pipelines")
                    .nodes
                    .expect("no pipeline nodes")
                    .pop()
                    .ok_or_else(|| {
                        (
                            mr.iid.clone(),
                            mr.title.clone(),
                            None,
                            "no pipeline".to_string(),
                        )
                    })?
                    .expect("no pipeline (2)")
                    .downstream
                    .expect("no downstream")
                    .nodes
                    .expect("no downstream nodes")
                    .pop()
                    .ok_or_else(|| {
                        (
                            mr.iid.clone(),
                            mr.title.clone(),
                            None,
                            "no downstream".to_string(),
                        )
                    })?
                    .expect("no downstream (2)");
                let trawler = downstream
                    .id
                    .split('/')
                    .last()
                    .unwrap_or_else(|| panic!("malformed id: {}", downstream.id))
                    .to_string();
                let trawler_finished = downstream.finished_at.ok_or_else(|| {
                    (
                        mr.iid.clone(),
                        mr.title.clone(),
                        Some(trawler.to_string()),
                        "no downstream finished_at".to_string(),
                    )
                })?;
                let report = trawler_pipeline_test_report(
                    &trawler,
                    job_token,
                    format!("mr !{}", mr.iid),
                    format!(
                        "https://gitlab.com/spade-lang/spade/-/merge_requests/{}",
                        mr.iid
                    ),
                )
                .await
                .map_err(|e| {
                    (
                        mr.iid.clone(),
                        mr.title.clone(),
                        Some(trawler.to_string()),
                        e,
                    )
                })?;
                Ok((mr.iid, mr.title, trawler, trawler_finished, report))
            })
            .collect::<Vec<_>>()
            .pipe(futures::future::join_all)
            .await;
        let details = mrs
            .iter()
            .filter_map(|x| match x {
                Ok((id, _title, _trawler, _trawler_date, (_report, details))) => {
                    Some((format!("mr-{id}"), details.clone()))
                }
                Err(_) => None,
            })
            .collect();
        let mrs = mrs
            .into_iter()
            .map(|x| match x {
                Ok((id, title, trawler, trawler_date, (report, _details))) => IndexMr {
                    id,
                    title,
                    report,
                    trawler,
                    trawler_date: chrono::DateTime::parse_from_rfc3339(&trawler_date)
                        .unwrap()
                        .date_naive()
                        .format("%Y-%m-%d")
                        .to_string(),
                    error: "".to_string(),
                },
                Err((id, title, trawler, err)) => IndexMr {
                    id,
                    title,
                    report: Report {
                        build: "-".to_string(),
                        test: "-".to_string(),
                        pnr: "-".to_string(),
                    },
                    trawler: trawler.unwrap_or_else(|| "-".to_string()),
                    trawler_date: "".to_string(),
                    error: err.to_string(),
                },
            })
            .collect();
        (mrs, details)
    }
}

impl CommitDownstreamPipelines {
    #[tracing::instrument(name = "CommitDownstreamPipelines::get", skip(job_token))]
    pub async fn get(job_token: Option<&str>) -> (Vec<IndexCommit>, Vec<(String, Details)>) {
        let query = Self::build_query(commit_downstream_pipelines::Variables {});
        let job_token_ = job_token.map(|s| s.to_string());
        let resp: Response<commit_downstream_pipelines::ResponseData> = smol::unblock(move || {
            ureq::post("https://gitlab.com/api/graphql")
                .pipe(|p| {
                    if let Some(job_token) = job_token_ {
                        p.set("JOB_TOKEN", &job_token)
                    } else {
                        p
                    }
                })
                .send_json(query)
                .unwrap()
                .into_string()
                .unwrap()
                .pipe_ref(|p| serde_json::from_str(p))
                .unwrap()
        })
        .await;
        let commits = resp
            .data
            .unwrap()
            .project
            .unwrap()
            .pipelines
            .unwrap()
            .nodes
            .unwrap()
            .into_iter()
            .map(|commit| async {
                let commit = commit.expect("no commit");
                let (title, sha) = {
                    let commit = commit.commit.expect("no commit");
                    (commit.title.expect("no commit title"), commit.sha)
                };
                let downstream = commit
                    .downstream
                    .expect("no downstream")
                    .nodes
                    .expect("no downstream nodes")
                    .pop()
                    .ok_or_else(|| {
                        (
                            title.clone(),
                            sha.clone(),
                            None,
                            "no downstream".to_string(),
                        )
                    })?
                    .expect("no downstream (2)");
                let trawler = downstream
                    .id
                    .split('/')
                    .last()
                    .unwrap_or_else(|| panic!("malformed id: {}", downstream.id))
                    .to_string();
                let trawler_finished = downstream.finished_at.unwrap();
                let report = trawler_pipeline_test_report(
                    &trawler,
                    job_token,
                    format!("commit {}", &sha[0..10]),
                    format!("https://gitlab.com/spade-lang/spade/-/commit/{}", sha),
                )
                .await
                .map_err(|e| (title.clone(), sha.clone(), Some(trawler.to_string()), e))?;
                Ok((title, sha, trawler, trawler_finished, report))
            })
            .collect::<Vec<_>>()
            .pipe(futures::future::join_all)
            .await;
        let details = commits
            .iter()
            .filter_map(|x| match x {
                Ok((_title, sha, _trawler, _trawler_date, (_report, details))) => {
                    Some((format!("commit-{}", sha), details.clone()))
                }
                Err(_) => None,
            })
            .collect();
        let commits = commits
            .into_iter()
            .map(|x| match x {
                Ok((title, sha, trawler, trawler_date, (report, _details))) => IndexCommit {
                    title,
                    sha,
                    report,
                    trawler,
                    trawler_date: chrono::DateTime::parse_from_rfc3339(&trawler_date)
                        .unwrap()
                        .date_naive()
                        .format("%Y-%m-%d")
                        .to_string(),
                    error: "".to_string(),
                },
                Err((title, sha, trawler, err)) => IndexCommit {
                    title,
                    sha,
                    report: Report {
                        build: "-".to_string(),
                        test: "-".to_string(),
                        pnr: "-".to_string(),
                    },
                    trawler: trawler.unwrap_or_else(|| "-".to_string()),
                    trawler_date: "".to_string(),
                    error: err.to_string(),
                },
            })
            .collect();
        (commits, details)
    }
}

#[derive(Debug, Deserialize)]
struct PipelineTestReport {
    test_suites: Vec<TestSuite>,
}

#[derive(Debug, Deserialize)]
struct TestSuite {
    test_cases: Vec<TestCase>,
    error_count: usize,
}

#[derive(Debug, Deserialize)]
struct TestCase {
    name: String,
    status: String,
    system_output: Option<String>,
}

#[tracing::instrument(skip(job_token))]
pub async fn trawler_pipeline_test_report(
    pipeline_id: &str,
    job_token: Option<&str>,
    title: String,
    external_link: String,
) -> Result<(Report, Details), String> {
    let url =
        format!("https://gitlab.com/api/v4/projects/38311985/pipelines/{pipeline_id}/test_report");
    tracing::debug!("url={url}");
    let job_token_ = job_token.map(|s| s.to_string());
    let pipeline_id_ = pipeline_id.to_string();
    let response = smol::unblock(move || -> Result<_, String> {
        let s = ureq::get(&url)
            .pipe(|p| {
                if let Some(job_token) = job_token_ {
                    p.set("JOB_TOKEN", &job_token)
                } else {
                    p
                }
            })
            .call()
            .map_err(|e| format!("error calling pipelines/{pipeline_id_}/test_report: {e}"))?
            .into_string()
            .unwrap();
        Ok(s)
    })
    .await?;

    let test_report: PipelineTestReport = serde_json::from_str(&response).map_err(|e| {
        format!("error converting response of pipelines/{pipeline_id}/test_report to json: {e}")
    })?;
    tracing::debug!("{test_report:#?}");

    let suite = test_report
        .test_suites
        .first()
        .ok_or_else(|| "no test suites".to_string())?;
    // per_test: Map (<name>) -> (Map (test|pnr) -> (success|failed, <system_output>))
    let mut per_test = HashMap::new();
    for tc in &suite.test_cases {
        tracing::debug!("{:?}", tc.name);
        if tc.status == "error" {
            per_test
                .entry(tc.name.as_str())
                .or_insert_with(HashMap::new)
                .insert("build", ("failed", tc.system_output.as_deref()));
        } else {
            let (name, test) = tc
                .name
                .split_once(' ')
                .ok_or_else(|| format!("error parsing test case name: {}", &tc.name))?;
            let test = &test[1..test.len() - 1];
            per_test
                .entry(name)
                .or_insert_with(HashMap::new)
                .insert(test, (&tc.status, tc.system_output.as_deref()));
        }
    }

    let cases = per_test
        .iter()
        .map(|(name, steps)| {
            let steps = steps
                .iter()
                .map(|(name, (status, output))| DetailsCaseStep {
                    name: name.to_string(),
                    success: *status == "success",
                    output: output.map(|s| s.to_string()).unwrap_or_else(String::new),
                })
                .sorted_by(|first, other| first.name.cmp(&other.name))
                .collect_vec();
            let steps_success = steps
                .iter()
                .filter(|DetailsCaseStep { success, .. }| *success)
                .count() as u32;
            let steps_total = steps.len() as u32;
            DetailsCase {
                name: name.to_string(),
                id: name.to_lowercase(),
                steps,
                steps_success,
                steps_total,
            }
        })
        .sorted_by(|first, other| first.id.cmp(&other.id))
        .collect_vec();
    let test_suc = cases
        .iter()
        .flat_map(|DetailsCase { steps, .. }| steps)
        .filter(|DetailsCaseStep { name, success, .. }| *name == "test" && *success)
        .count();
    let test_fail = cases
        .iter()
        .flat_map(|DetailsCase { steps, .. }| steps)
        .filter(|DetailsCaseStep { name, success, .. }| *name == "test" && !*success)
        .count();
    let pnr_suc = cases
        .iter()
        .flat_map(|DetailsCase { steps, .. }| steps)
        .filter(|DetailsCaseStep { name, success, .. }| *name == "pnr" && *success)
        .count();
    let pnr_fail = cases
        .iter()
        .flat_map(|DetailsCase { steps, .. }| steps)
        .filter(|DetailsCaseStep { name, success, .. }| *name == "pnr" && !*success)
        .count();

    let total = per_test.len() + suite.error_count;
    let build_suc = total - suite.error_count;
    let build = format!("{}/{}", build_suc, total);
    let test = format!("{}/{}", test_suc, test_suc + test_fail);
    let pnr = format!("{}/{}", pnr_suc, pnr_suc + pnr_fail);

    Ok((
        Report { build, test, pnr },
        Details {
            title,
            external_link,
            generated: GENERATED.clone(),
            cases,
        },
    ))
}
